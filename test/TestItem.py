import sys
sys.path.append("../src/")

import User as U
import Item as I

######-----  Test for __init__(self, owner, type_, title, uniqId, artist, genre, year): -----#####
user_1 = U.User("berkantbayraktar06@gmail.com","Berkant Bayraktar","316269")
user_2 = U.User("yemresacma@gmail.com","Yunus Emre Sacma","123456")
user_3 = U.User("eli@gmail.com","Agana Nigi","324123")


Item_1 = I.Item(user_1, I.ItemType.BOOK, "Yalanci Iliskiler", None, "Tolstoy", "drama", "1996")
Item_2 = I.Item(user_2, I.ItemType.BOOK, "Silahlara Veda", None, "HEMINGWAY", "drama", "1999")

Item_3 = I.Item(user_1, I.ItemType.BOOK, None, 9789751000125, None, None, None)
Item_4 = I.Item(user_2, I.ItemType.BOOK, None, 9784770027955, None, None, None)

print(str(Item_1))
print(str(Item_2))
print(str(Item_3))
print(str(Item_4))
print("-------------------------")
######-----  Test for borrowedreq(self, user): -----#####

Item_1.borrowed_req(user_2)
Item_1.borrowed_req(user_3)
''' Item_1.borrowedreq(user_3) # Check for duplicate request => throws exception'''



print(Item_1.requests)
print("-------------------------")
######-----  Test for borrowed_by(self, user, returnDate = "+2 weeks"): -----#####
######----- Test for returned(self, location = None): -----#####
user_2.friend(user_1.email)
user_3.friend(user_1.email)
user_1.set_friend(user_2, U.FriendshipStatus.CLOSE_FRIEND)
user_1.set_friend(user_3, U.FriendshipStatus.FRIEND)
user_1.friend(user_2.email)
user_1.friend(user_3.email)
user_2.set_friend(user_1, U.FriendshipStatus.CLOSE_FRIEND)
user_3.set_friend(user_1, U.FriendshipStatus.CLOSE_FRIEND)
Item_1.setstate(I.AccessType.BORROW, I.Access.EVERYONE)

print(Item_1.available)

Item_1.borrowed_by(user_2, "+2 weeks")
print(Item_1.available)
Item_1.returned()
Item_1.borrowed_by(user_2, "+14 days")
print(Item_1.available)
Item_1.returned()

print("-------------------------")

######----- Test for comment(self, user, commentText): -----#####
######----- Test for list_comments(self): -----#####

'''Item_1.comment(user_1.commentText("user_1 first comment for Item_1")) => throws exception'''

Item_1.comment(user_2, "user_2's first comment for Item_1")
Item_1.comment(user_3, "user_3's first comment for Item_1")
Item_1.comment(user_2, "user_2's second comment for Item_1")

for i in Item_1.list_comments():
    print(i, Item_1.list_comments()[i])

print("-------------------------")
######----- Test for rate(self, user, rating): -----#####
######-----  Test for getrating(self): -----#####

'''Item_1.rate(user_1, 9) => throws exception'''

Item_1.rate(user_2, 9)
Item_1.rate(user_3, 9)

print(Item_1.get_rating())

print("-------------------------")

######-----  Test for locate(): -----#####
Item_1.locate(I.Location.BOARD)
print(Item_1.location is I.Location.BOARD)

Item_1.locate(I.Location.SHELF)
print(Item_1.location is I.Location.SHELF)

Item_1.locate(I.Location.ROOM)
print(Item_1.location is I.Location.ROOM)

print("-------------------------")

######-----  Test for is_accessible and setstate(): -----#####
print(I.Item.is_accessible(U.FriendshipStatus.NO_FRIEND, I.Access.CLOSED) is False)
print(I.Item.is_accessible(U.FriendshipStatus.FRIEND, I.Access.CLOSED) is False)
print(I.Item.is_accessible(U.FriendshipStatus.CLOSE_FRIEND, I.Access.CLOSED) is False)
print(I.Item.is_accessible(U.FriendshipStatus.NO_FRIEND, I.Access.CLOSE_FRIENDS) is False)
print(I.Item.is_accessible(U.FriendshipStatus.FRIEND, I.Access.CLOSE_FRIENDS) is False)
print(I.Item.is_accessible(U.FriendshipStatus.NO_FRIEND, I.Access.FRIENDS) is False)
print(I.Item.is_accessible(U.FriendshipStatus.FRIEND, I.Access.FRIENDS))
print(I.Item.is_accessible(U.FriendshipStatus.CLOSE_FRIEND, I.Access.FRIENDS))
print(I.Item.is_accessible(U.FriendshipStatus.CLOSE_FRIEND, I.Access.CLOSE_FRIENDS))
print(I.Item.is_accessible(U.FriendshipStatus.FRIEND, I.Access.EVERYONE))
print(I.Item.is_accessible(U.FriendshipStatus.NO_FRIEND, I.Access.EVERYONE))
print(I.Item.is_accessible(U.FriendshipStatus.CLOSE_FRIEND, I.Access.EVERYONE))

print("-------------------------")

######-----  Test for search(self, user, searchText, genre, year, forBorrow = False): -----#####

print("SEARCH TESTING...")

''' Expected output

'''
lst = I.Item.search(user_1, "tolstoy", None, "1992", False)
for i in lst:
    print(i[0].title, i[1].email)  # excepted output -> yalanci iliskiler berkantbayraktar06@gmail.com
print("")

''' Expected output
yalanci iliskiler berkantbayraktar06@gmail.com
çalıkuşu berkantbayraktar06@gmail.com
'''
lst = I.Item.search(user_1, "tolstoy", None,  1999, False)
for i in lst:
    print(i[0].title, i[1].email)  # excepted output -> yalanci iliskiler berkantbayraktar06@gmail.com
print("")

''' Expected output
yalanci iliskiler berkantbayraktar06@gmail.com
silahlara veda berkantbayraktar06@gmail.com
'''
lst = I.Item.search(user_1, "tolstoy", None,  2004, False)
for i in lst:
    print(i[0].title, i[1].email)  # excepted output -> yalanci iliskiler berkantbayraktar06@gmail.com
print("")

''' Expected output
yalanci iliskiler berkantbayraktar06@gmail.com
星の王子さま berkantbayraktar06@gmail.com
'''
lst = I.Item.search(user_1, None, None,  1992, False)
for i in lst:
    print(i[0].title, i[1].email)  # excepted output -> yalanci iliskiler berkantbayraktar06@gmail.com
print("")

''' Expected output
çalıkuşu berkantbayraktar06@gmail.com
'''
lst = I.Item.search(user_1, None, None,  None, False)
for i in lst:
    print(i[0].title, i[1].email)  # excepted output -> yalanci iliskiler berkantbayraktar06@gmail.com
print("")

''' Expected output
None
'''
lst = I.Item.search(user_1, "Tolstoy", None,  2004, True)
for i in lst:
    print(i[0].title, i[1].email)  # excepted output -> yalanci iliskiler berkantbayraktar06@gmail.com
print("")
print("END SEARCH TESTING...")

print("-------------------------")

######-----  Test for watch(self, user, watch_method): -----#####
Item_1.setstate(I.AccessType.DETAIL, I.Access.EVERYONE)
Item_1.setstate(I.AccessType.BORROW, I.Access.EVERYONE)
Item_1.setstate(I.AccessType.SEARCH, I.Access.EVERYONE)
Item_1.setstate(I.AccessType.COMMENT, I.Access.EVERYONE)
Item_1.setstate(I.AccessType.VIEW, I.Access.EVERYONE)

Item_1.watch(user_2, I.WatchMetkhod.WATCH_BORROW)
Item_1.watch(user_3, I.WatchMethod.WATCH_COMMENT)
''' Expected output
None -> since there is no access right
'''
print(Item_1.watchList, "\n")


Item_1.watch(user_2, I.WatchMethod.WATCH_BORROW)
Item_1.watch(user_3, I.WatchMethod.WATCH_COMMENT)
print(Item_1.watchList)

print("-------------------------")

######-----  Test for view(self, user): -----#####

print(Item_1.view(user_2))
print(Item_1.detail(user_2))

print("-------------------------")

######-----  Test for announce(self, type_, msg): -----#####

user_1.friendshipStatus.clear()
user_2.friendshipStatus.clear()
user_3.friendshipStatus.clear()
Item_1.watchList.clear()

Item_1.announce(U.FriendshipStatus.CLOSE_FRIEND, "HEY THIS ITEM IS THE BEST")
print("user2: ", user_2.notificationList)
print("user3: ", user_3.notificationList)

Item_1.watch(user_2,I.WatchMethod.WATCH_COMMENT)
Item_1.announce(U.FriendshipStatus.CLOSE_FRIEND, "HEY THIS ITEM IS THE BEST")
print("user2: ", user_2.notificationList)
print("user3: ", user_3.notificationList)
user_2.notificationList.clear()

user_3.friend(user_1.email)
user_1.set_friend(user_3, U.FriendshipStatus.CLOSE_FRIEND)

Item_1.watch(user_2,I.WatchMethod.WATCH_COMMENT)
Item_1.announce(U.FriendshipStatus.CLOSE_FRIEND, "HEY THIS ITEM IS THE BEST")
print("user2: ", user_2.notificationList)
print("user3: ", user_3.notificationList)

######-----  Test for delete(self): -----#####

for i in I.Item.ItemList:
    if i.uniqId == Item_1.uniqId:
        print("FOUND")
        break

Item_1.delete()

x = 0
for i in I.Item.ItemList:
    if i.uniqId == Item_1.uniqId:
        x = 1

if x == 1:
    print("FOUND")
else:
    print("DELETED")


