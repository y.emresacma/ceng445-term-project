import sys
sys.path.append("../src/")

import User as U
import Item as I

U.User.loadClassVariables()

U.User.printUserList()

for myUser in U.User.userList:
    print("Friendship Status of " + myUser.nameSurname + ": ", myUser.friendshipStatus)
