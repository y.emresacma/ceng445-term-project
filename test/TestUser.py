import sys
sys.path.append("../src/")

import User as U
import Item as I

######-----  Test for __init__(self, email, nameSurname, password): -----#####

myUser = U.User("berkantbayraktar06@gmail.com","Berkant Bayraktar","bbayraktar1995")
myUser2 = U.User("yemresacma@gmail.com","Yunus Emre Sacma","ysacma1996")
myUser3 = U.User("ilkeraycicek36@hotmail.com","Ilker Aycicek","iaycicek1996")
myUser4 = U.User("berkeracir@hotmail.com","Berker Acir","bacir1996")
myUser5 = U.User("ceng@metu.edu.tr","CENG","metuceng")
U.User.printUserList()

print("------------------------------------------")

######-----  Test for verify(email, verificationNum) -----#####

U.User.verify("berkantbayraktar06@gmail.com",myUser.verificationCode)
U.User.verify("yemresacma@gmail.com",myUser2.verificationCode)
U.User.verify("ilkeraycicek36@hotmail.com",myUser3.verificationCode)
U.User.verify("berkeracir@hotmail.com",myUser4.verificationCode)
U.User.verify("ceng@metu.edu.tr",myUser5.verificationCode)
U.User.printUserList()

print("------------------------------------------")

######-----  Test for verify(email, verificationNum) -----#####
#case1
myUser.changePassword("1907","bbayraktar1995")
#case2
myUser2.changePassword("1903",None)
myUser2.changePassword("1903",myUser2.password)

U.User.printUserList()

print("------------------------------------------")

######-----  Test for lookup(self, emailList) -----#####
mailList = ["ilkeraycicek36@hotmail.com","ceng@metu.edu.tr","berkantbayraktar06@gmail.com","serhatbayraktar2001@hotmail.com","yemresacma@gmail.com"]
matchingMails = U.User.lookup(mailList)
print("Matched mails: ",matchingMails)

print("------------------------------------------")

######-----  Test for friend(self,email) -----#####
myUser.friend("yemresacma@gmail.com")
myUser.friend("ilkeraycicek36@hotmail.com")
myUser.friend("berkeracir@hotmail.com")

myUser2.friend("ilkeraycicek36@hotmail.com")
myUser2.friend("berkeracir@hotmail.com")

myUser3.friend("berkeracir@hotmail.com")



#check whether friendship request is sent or not 
print("Friendship Request List of " + myUser.nameSurname + ": ", myUser.friendshipRequests)
print("Friendship Request List of " + myUser2.nameSurname + ": ", myUser2.friendshipRequests)
print("Friendship Request List of " + myUser3.nameSurname + ": ", myUser3.friendshipRequests)
print("Friendship Request List of " + myUser4.nameSurname + ": ", myUser4.friendshipRequests)

print("------------------------------------------")

######-----  Test for set_friend(self, user, state) -----#####

#case 1 CLOSE_FRIEND OR FRIEND
myUser2.set_friend(myUser,U.FriendshipStatus.CLOSE_FRIEND)
myUser3.set_friend(myUser,U.FriendshipStatus.FRIEND)
myUser3.set_friend(myUser2,U.FriendshipStatus.FRIEND)
myUser4.set_friend(myUser,U.FriendshipStatus.NO_FRIEND)
myUser4.set_friend(myUser2,U.FriendshipStatus.FRIEND)
myUser4.set_friend(myUser3,U.FriendshipStatus.FRIEND)


print("Friendship Status of " + myUser.nameSurname + ": ", myUser.friendshipStatus)
print("Friendship Status of " + myUser2.nameSurname + ": ", myUser2.friendshipStatus)
print("Friendship Status of " + myUser3.nameSurname + ": ", myUser3.friendshipStatus)
print("Friendship Status of " + myUser4.nameSurname + ": ", myUser4.friendshipStatus)



print("------------------------------------------")

######-----  Test for listItems(self, user) -----#####

Item_1 = I.Item(myUser, I.ItemType.BOOK, "Yalanci Iliskiler", None, "Tolstoy", "drama", "1996")
Item_2 = I.Item(myUser, I.ItemType.BOOK, "Silahlara Veda", None, "HEMINGWAY", "drama", "1999")

Item_3 = I.Item(myUser2, I.ItemType.BOOK, None, 9789751000125, None, None, None) # 
Item_4 = I.Item(myUser2, I.ItemType.BOOK, None, 9784770027955, None, None, None) # 

Item_5 = I.Item(myUser3, I.ItemType.BOOK, None, 9788498383621, None, None, None) #
Item_6 = I.Item(myUser3, I.ItemType.BOOK, None, 9789750803314, None, None, None) # 

Item_7 = I.Item(myUser4, I.ItemType.BOOK, None, 9789753421812, None, None, None) # 
Item_8 = I.Item(myUser4, I.ItemType.BOOK, None, 9789753422024, None, None, None) # 

print("Item 1: ", Item_1)
print("Item 2: ", Item_2)
print("Item 3: ", Item_3)
print("Item 4: ", Item_4)
print("Item 5: ", Item_5)
print("Item 6: ", Item_6)
print("Item 7: ", Item_7)
print("Item 8: ", Item_8)

Item_1.setstate(I.AccessType.VIEW,I.Access.FRIENDS)
Item_2.setstate(I.AccessType.VIEW,I.Access.FRIENDS)
Item_3.setstate(I.AccessType.VIEW,I.Access.CLOSE_FRIENDS)
Item_4.setstate(I.AccessType.VIEW,I.Access.FRIENDS)
Item_5.setstate(I.AccessType.VIEW,I.Access.CLOSE_FRIENDS)
Item_6.setstate(I.AccessType.VIEW,I.Access.FRIENDS)
Item_7.setstate(I.AccessType.VIEW,I.Access.EVERYONE)
Item_8.setstate(I.AccessType.VIEW,I.Access.FRIENDS)

Item_1.setstate(I.AccessType.BORROW,I.Access.EVERYONE)
Item_2.setstate(I.AccessType.BORROW,I.Access.EVERYONE)


i = 1 
print()
itemList = myUser.listItems(myUser2)
print("listItems method from ",myUser.email , " to ", myUser2.email  ," :")
for item in itemList:
    print ("Item number ",i ," :",item)
    i +=1
print()

i = 1 
itemList = myUser.listItems(myUser3)
print("listItems method from ",myUser.email , " to ", myUser3.email  ," :")

for item in itemList:
    print ("Item number ",i ," :",item)
    i +=1
print()

i = 1 
itemList = myUser.listItems(myUser4)
print("listItems method from ",myUser.email , " to ", myUser4.email  ," :")

for item in itemList:
    print ("Item number ",i ," :",item)
    i +=1
print()

'''
myUser.watch(myUser2,I.WatchMethod.WATCH_BORROW)
it = I.Item(myUser2,I.ItemType.DVD,"Kurtlar Vadisi",None,"Polat Alemdar","Action","2003")
it.setstate(I.AccessType.BORROW,I.Access.EVERYONE)
print("Notification List of ", myUser.email , myUser.notificationList)'''
U.User.saveClassVariables()
I.Item.saveClassVariables()