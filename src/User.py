from enum import Enum
import random
import re
import Item as I
import pickle 
import os 

class User:

    verificationMap = dict()
    userList = list()
    '''
    A new user is created. User is in notverified
    state. User is send an email with verification
    number.
    '''
    @staticmethod
    def save(o, filepath):
        with open(filepath,"wb") as f:
            pickle.dump(o,f)
    @staticmethod
    def load (filepath):
        with open(filepath,"rb") as f:
            return pickle.load(f)
    @staticmethod
    def loadClassVariables():
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(dir_path,"verificationmap.p")
        
        with open(file_path,"rb") as f:
            User.verificationMap = pickle.load(f)
        
        file_path = os.path.join(dir_path,"userlist.p")

        with open(file_path,"rb") as f:
            User.userList = pickle.load(f)

    @staticmethod
    def saveClassVariables():
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(dir_path,"verificationmap.p")

        with open(file_path,"wb") as f:
            pickle.dump(User.verificationMap,f)
        
        file_path = os.path.join(dir_path,"userlist.p")

        with open(file_path,"wb") as f:
            pickle.dump(User.userList,f)

    def __init__(self, email, nameSurname, password):
        self.email = email
        self.nameSurname = nameSurname
        self.password = password
        self.userState = UserState.NOTVERIFIED
        self.verificationCode = generateVerificationCode()
        self.friendshipStatus = dict()  # User email -> FriendshipStatus pair
        self.friendshipRequests = list()
        self.notificationList = list()  # Message box
        self.watchList = dict()

        User.verificationMap[email] = self.verificationCode
        User.userList.append(self)
    '''
    Static method. Verification number is checked
    agains stored one and user is enabled.
    '''
    @staticmethod
    def verify(email, verificationNum):
        try:
            if(User.verificationMap[email] == verificationNum):
                for user in User.userList:
                    if(user.email == email):
                        user.userState = UserState.VERIFIED
                        print("Now , you are verified user with userState : ",user.userState)
                        break
            else:
                print("your e-mail and verification do NOT match.")
                        
                        

        except:
            print("e-mail cannot be found on database")

    '''
    User password is changed. If forgotten,
    oldpassword is set as None and a reminder
    with a uniq temporary password is sent. Next
    call will be with temporary password as
    oldpassword for changing the password.
    '''


    def changePassword(self, newPass, oldPass = None):
        if(oldPass == None):  #forgotten password 
            self.password = generateTemporaryPassword()   #generate Temporary Password and set it as password
            print("Hey " + self.nameSurname + ", your temporary password is ",self.password)

        else:  # standart password change
            if(self.password == oldPass):
                self.password = newPass
                print("Hey "+ self.nameSurname + ", your password is changed successfully.")
            else:
                print("you entered incorrect password")
    '''
    A list of emails are given and looked in the
    user database, matching emails are returned.
    '''
    @staticmethod
    def lookup(emailList):   # Maybe, this method should be static !!!!!!!!!!!!!!!!!!
        matchingEmails = list()
        for user in User.userList:
            for email in emailList:
                if(user.email == email):
                    matchingEmails.append(email)

        return matchingEmails
    '''
    Send a friendship request to the user with
    given email.
    '''
    def friend(self,email):
        for user in User.userList:
            if(user.email == email):
                user.friendshipRequests.append(self.email)
                print("Hey " + self.nameSurname + " your friendship request is sent to " + user.nameSurname + " successfully.")

    '''
    Change friend status with user, either
    nofriend, friend, closefriend
    '''
    def set_friend(self, user, state):
        if user.email in self.friendshipRequests:
            if state == FriendshipStatus.FRIEND or state == FriendshipStatus.CLOSE_FRIEND:
                self.friendshipStatus[user.email] = state
                user.friendshipStatus[self.email] = state
                print(user.nameSurname + " is added your friend list as " + str(state))

            elif state == FriendshipStatus.NO_FRIEND:
                self.friendshipRequests.remove(user.email)
                print("You rejected the friendship request of " + user.nameSurname)
        
        else:
            print("There is no exist such a friend request.")

    '''
    Get item list of the user given as parameter.
    Only items with view permissions are listed.
    '''
    def listItems(self, user):
        returnList = list()

        for item in I.Item.ItemList:
            if(item.owner.email == user.email and I.Item.is_accessible(self.get_friendship(item.owner.email),item.accessStates[I.AccessType.VIEW])):
                returnList.append(item)

        return returnList

    '''
    Watch user for new items. user should be
    friend of current user object. One of detail
    or borrow permission is required for the items.
    '''
    def watch(self, user, watchMethod): # Might need to be rechecked

        try:
            if(watchMethod == None):
                del watchMethod[user.email]
            else:    
                if(self.is_friend(user.email)):
                    if watchMethod == I.WatchMethod.WATCH_COMMENT:
                        self.watchList[user.email] = I.WatchMethod.WATCH_COMMENT
                    
                    elif watchMethod == I.WatchMethod.WATCH_BORROW:
                        self.watchList[user.email] = I.WatchMethod.WATCH_BORROW
                    
        except KeyError:
            pass

    # a function for validating an Email 
    @staticmethod
    def is_valid(email):
        regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'  
        # pass the regualar expression 
        # and the string in search() method 
        if (re.search(regex,email)):
            return True 
        return False      

    def is_friend(self, userEmail):
        try:
            if self.friendshipStatus[userEmail] == FriendshipStatus.FRIEND or self.friendshipStatus[userEmail] == FriendshipStatus.CLOSE_FRIEND:
                return True
            return False
        except KeyError:
            return False
    
    def get_friendship(self, user_email):
        try:
            return self.friendshipStatus[user_email]
        except KeyError:
            return FriendshipStatus.NO_FRIEND

    def add_notification(self, msg):
        self.notificationList.append(msg)

    #  Overload str operator
    def __str__(self):
        return (self.email + ', ' +
                self.nameSurname + ',' + 
                self.password + ', ' +
                str(self.userState) + ', ' + 
                str(self.verificationCode) + ', ' +
                str(self.friendshipStatus)
                )

    @staticmethod
    def printUserList():
        for user in User.userList:
            print("e-mail: ", user.email )
            print("fullName: ", user.nameSurname)
            print("password: ", user.password)
            print("accountState : ", user.userState)
            try:
                print("verification code : ", user.verificationCode)
            except:
                pass
            print("------------------")
        

class UserState(Enum):
    NOTVERIFIED = 0
    VERIFIED  = 1 


class FriendshipStatus(Enum):
    NO_FRIEND = 0
    FRIEND = 1
    CLOSE_FRIEND = 2

def generateVerificationCode():
    return str(random.randrange(100000, 999999))

def generateTemporaryPassword():
    return str(random.randrange(100000000, 999999999))