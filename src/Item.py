import itertools
from enum import Enum
from datetime import datetime
from datetime import timedelta
import urllib.request
import json
import textwrap
import User as U
import os
import pickle

class Item:
    ItemList = list()
    id_iter = itertools.count()

    @staticmethod
    def loadClassVariables():
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(dir_path,"itemlist.p")
        
        with open(file_path,"rb") as f:
            Item.ItemList = pickle.load(f)
        
        file_path = os.path.join(dir_path,"iditer.p")

        with open(file_path,"rb") as f:
            Item.id_iter = pickle.load(f)

    @staticmethod
    def saveClassVariables():
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(dir_path,"itemlist.p")

        with open(file_path,"wb") as f:
            pickle.dump(Item.ItemList,f)
        
        file_path = os.path.join(dir_path,"iditer.p")

        with open(file_path,"wb") as f:
            pickle.dump(Item.id_iter,f)

    def __init__(self, owner, type_, title,
                uniqId, artist, genre, year):
        if uniqId is None:
            self.title = title  # to be able to help searching
            self.artist = artist  # to be able to help searching
            self.genre = genre
            self.year = year
        else:
            self.title, self.year, self.artist, self.genre = Item.retrieve_book_data(uniqId)

        self.ID = next(Item.id_iter)
        self.owner = owner
        self.type_ = type_
        self.location = None
        self.uniqId = uniqId

        Item.ItemList.append(self)
        self.requests = list()  # user emails queue for borrow request
        self.accessStates = dict()  # stateType -> state
        self.comments = dict()  # user email -> comment text
        self.rates = dict()  # user email -> rate
        self.watchList = dict()  # user emails -> WatchMethod
        self.available = (True, None, None)  # bool-> indicates item is available or not, second one indicates return date
                                           # third one is User object who borrow item

        self.accessStates[AccessType.VIEW] = Access.CLOSED
        self.accessStates[AccessType.DETAIL] = Access.CLOSED
        self.accessStates[AccessType.BORROW] = Access.CLOSED
        self.accessStates[AccessType.COMMENT] = Access.CLOSED
        self.accessStates[AccessType.SEARCH] = Access.CLOSED

    '''Add a borrow request for user. Requests are
    queued. The order in the queue is returned'''
    def borrowed_req(self, user):
        if user.email not in self.requests:
            self.requests.append(user.email)
            return self.requests
        else:
            raise Exception("User already in request queue")

    def borrowed_by(self, user, return_date = "+2 weeks"):
        if self.available[0] or self.available[1] < datetime.today():
            if Item.is_accessible(user.get_friendship(self.owner.email), self.accessStates[AccessType.BORROW]):
                if user.email in self.requests:
                    date = datetime.today()
                    if return_date[3:] == "days":
                        date += timedelta(days=int(return_date.split()[0][1:]))
                    else:
                        date += timedelta(days=int(return_date.split()[0][1:])*7)
                    self.available = (False, date, user)
                else:
                    raise Exception("There is no borrow request from " + user.email)
            else:
                raise Exception("Item cannot borrowed because of access rights")
        else:
            raise Exception("Item is currently borrowed by someone else")
        pass

    # Make it available
    def returned(self, location = None):
        self.location = location
        self.available = (True, None, None)

    def comment(self, user, comment_text):  # Adds comment
        if self.owner.email != user.email:
            if self.owner.is_friend(user.email):
                self.comments[user.email] = comment_text
            else:
                raise Exception("Only friends can comment on item")
        else:
            raise Exception("Users cannot comment on their own item")

    def list_comments(self):
        return self.comments

    def rate(self, user, rating):
        if user.email not in self.rates:
            if self.owner.is_friend(user.email):
                self.rates[user.email] = rating
            else:
                raise Exception("Only friends can rate item")
        else:
            raise Exception("User has already rated this item")

    # Returns avg rating
    def get_rating(self):
        sum_ = 0.0
        size = 0
        try:
            for key in self.rates:
                sum_ += self.rates[key]
                size += 1
        except:
            raise Exception("No rates")
        return sum_ / size

    def locate(self, location):
        self.location = location

    '''state_type is either of view, detail,
    borrow, comment, search and state is
    either of closed, everyone, friends,
    closefriends'''
    # Parameters are enum
    def setstate(self, state_type, state):
        self.accessStates[state_type] = state
        if state_type == AccessType.BORROW or state_type == AccessType.COMMENT:
            for user_mail in self.watchList:
                for user in U.User.userList:
                    if user.email == user_mail:
                        user.add_notification(self.owner.email + "'s " + str(state_type) + " changed to " + str(state))

        for user in U.User.userList:
            for watchUser in user.watchList:
                if(watchUser == self.owner.email):
                        if( (Item.is_accessible(self.owner.get_friendship(watchUser), self.accessStates[AccessType.DETAIL]) or
                        Item.is_accessible(self.owner.get_friendship(watchUser), self.accessStates[AccessType.BORROW])) and 
                        self.owner.email == watchUser):
                            user.notificationList.append(str(self.title) + " is available for "+ str(user.watchList[watchUser]))

    @staticmethod
    def search(user, search_text, genre, year, for_borrow=False):  # if year is range the format will be like 'year'-'year'
        search_result = list()  # Item-User pairs (return value)

        search_list = list()
        try:
            search_list = [x.lower() for x in search_text.split(",")]  # Words that will be searched, splitted by comma into list
        except AttributeError:
            pass

        # We are making year a range in both case since it makes easier to compare years
        try:
            # Make year tuple
            if type(year) is int:
                year = str(year)

            if year.find('-') == -1: # not range
                year = (int(year), int(year))
            else:  # year is range
                year = (int(year[:year.find('-')]), int(year[year.find('-') + 1:]))
        except:
            year = (0, 0)

        if not for_borrow:
            for item in Item.ItemList:
                try:
                    for search in search_list:
                        if (item.title is not None and item.title.lower() == search) or (item.artist is not None and item.artist.lower() == search):
                            search_result.append((item, user))
                            break

                    if (year[0] <= int(item.year) <= year[1]) or (item.genre is not None and genre == item.genre.lower()):
                        search_result.append((item, user))
                except KeyError:
                    continue

        elif for_borrow:
            for item in Item.ItemList:
                try:
                    if Item.is_accessible(item.owner.friendshipStatus[user.email], item.accessStates[AccessType.BORROW]):
                        for search in search_list:
                            if (item.title is not None and item.title.lower() == search) or (item.artist is not None and item.artist.lower() == search):
                                search_result.append((item, user))
                                break

                        if (year[0] <= int(item.year) <= year[1]) or (item.genre is not None and genre == item.genre.lower()):
                            search_result.append((item, user))
                except KeyError:
                    continue

        return list(set(search_result))  # Make list unique

    '''Watching comments require detail access, 
    borrow status requires detail borrow access.'''
    def watch(self, user, watch_method):
        try:
            if watch_method == WatchMethod.WATCH_COMMENT:
                if Item.is_accessible(self.owner.get_friendship(user.email), self.accessStates[AccessType.DETAIL]):
                    self.watchList[user.email] = watch_method
                else:
                    raise Exception("Watching comments require detail access")
            else:
                if Item.is_accessible(self.owner.get_friendship(user.email), self.accessStates[AccessType.BORROW]):
                    self.watchList[user.email] = watch_method
                else:
                    raise Exception("Borrow status requires detail borrow access")
        except KeyError:
            pass

    '''Returns list of tuples for summary information
    "owner" -> owner email
    "type" -> type_
    "title" -> title'''
    def view(self, user):  # TODO User not used for now. Nothing mentioned about it in the text
        sum_info = list()
        sum_info.append(("Owner", self.owner))
        sum_info.append(("Type", self.type_))
        sum_info.append(("Title", self.title))

        return sum_info

    '''Returns list of tuples for detailed information
    "owner" -> Owner email
    "type" -> type_
    "title" -> title
    "artist" -> artist
    "genre" -> genre
    "year" -> year
    "Comments -> comments"
    "Rate" -> Avg rate
    '''
    def detail(self, user):
        detailed_info = list()
        detailed_info.append(("Owner", self.owner))
        detailed_info.append(("Type", self.type_))
        detailed_info.append(("Title", self.title))
        detailed_info.append(("Artist", self.artist))
        detailed_info.append(("Genre", self.genre))
        detailed_info.append(("Year", self.year))
        detailed_info.append(("Comments", self.comments))
        detailed_info.append(("Rate", self.get_rating()))

        if user.email == self.owner.email:
            detailed_info.append(("Location", self.location))

        return detailed_info

    #  type is friendship type
    def announce(self, type_, msg):
        for user in U.User.userList:
            if user.email in self.watchList or user.get_friendship(self.owner.email) == type_:
                user.add_notification(msg)

    def delete(self):
        self.requests.clear()
        self.accessStates.clear()
        self.comments.clear()
        self.rates.clear()

        for user in U.User.userList:
            if user in self.watchList:
                user.add_notification("Item with the id " + str(self.uniqId) + " is deleted")
        self.watchList.clear()

        for i in range(len(self.ItemList) - 1, -1, -1):
            if self.ItemList[i].uniqId == self.uniqId:
                del self.ItemList[i]

    def __str__(self):
        return (self.owner.email + ', ' +
                str(self.type_) + ', ' +
                self.title + ' ' +
                str(self.uniqId) + ' ' +
                self.artist + ', ' +
                str(self.genre) + ', ' +
                str(self.year)
                )

    @staticmethod
    def retrieve_book_data(uniqId):
        base_api_link = "https://www.googleapis.com/books/v1/volumes?q=isbn:"
        uniqId = str(uniqId)
        with urllib.request.urlopen(base_api_link + uniqId) as f:
            text = f.read()

        decoded_text = text.decode("utf-8")
        obj = json.loads(decoded_text) # deserializes decoded_text to a Python object

        try:
            volume_info = obj["items"][0]
        except KeyError:
            raise Exception("Invalid id")

        try:
            title = volume_info["volumeInfo"]["title"]
        except KeyError:
            title = None

        try:
            year = volume_info["volumeInfo"]["publishedDate"]
            if year.find('-'):
                year = year[:4] + year[:2] + year[5:7]  # example '2004-08' -> 2004-2008
        except KeyError:
            year = None

        try:
            author = volume_info["volumeInfo"]["authors"][0]
        except KeyError:
            author = None

        try:
            categories = volume_info["volumeInfo"]["categories"][0]
        except KeyError:
            categories = None

        return title, year, author, categories

    @staticmethod # Helper
    def is_accessible(friendship_status, access):
        if access == Access.EVERYONE:
            return True

        if access != Access.CLOSED and (friendship_status.value + 1 >= access.value):
            return True

        return False


class Access(Enum):
    CLOSED = 0
    EVERYONE = 1
    FRIENDS = 2
    CLOSE_FRIENDS = 3


class AccessType(Enum):
    VIEW = 0
    DETAIL = 1
    BORROW = 2
    COMMENT = 3
    SEARCH = 4


# corresponds to type_ variable
class ItemType(Enum):
    DVD = 0
    CD = 1
    BOOK = 2


class WatchMethod(Enum):
    WATCH_COMMENT = 0
    WATCH_BORROW = 1


class Location(Enum):
    ROOM = 0
    BOARD = 1
    SHELF = 2

'''
    Exception Notes
    * Same user cannot be added to borrow request twice

    * Users cannot comment their own items

    * Users cannot rate an item twice
'''