from socket import *
from threading import Thread
import time
import random
import pickle

import User as U
import Item as I

PORT = 20445


def find_user(email):
    U.User.loadClassVariables()
    for user in U.User.userList:
        if user.email == email:
            return user


def user_watch(c):
    answer = input("Do you want to watch a user for new items? Press Y/N : ")

    if answer == "Y" or answer == "y":

        email = input("Enter an email address to be able to watch the user that you want: ")
        user_object = find_user(email)

        time.sleep(random.random()*3)
        c.send(pickle.dumps("2"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(U.User.watch))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.WatchMethod.WATCH_BORROW))


def item_watch(c):
    answer = input("Do you want to watch a item for status changes ? Press Y/N : ")
    if answer == "Y" or answer == "y":

        email = input("Enter an email address to be able to watch the user that you want: ")
        user_object = find_user(email)

        time.sleep(random.random() * 3)
        c.send(pickle.dumps("2"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.watch))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.WatchMethod.WATCH_BORROW))


def create_item(c):
    answer = input("Do you want to add item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        answer = input("Do you want to add manually ? Press Y/N : ")

        if answer == "Y" or answer == "y":
            time.sleep(random.random()*3)
            c.send(pickle.dumps("6"))

            time.sleep(random.random()*3)
            c.send(pickle.dumps(I.Item.__init__))

            time.sleep(random.random()*3)
            c.send(pickle.dumps(I.ItemType.BOOK))

            time.sleep(random.random()*3)
            c.send(pickle.dumps("The Origin"))

            time.sleep(random.random()*3)
            c.send(pickle.dumps(None))

            time.sleep(random.random()*3)
            c.send(pickle.dumps("Dan Brown"))

            time.sleep(random.random()*3)
            c.send(pickle.dumps("action"))

            time.sleep(random.random()*3)
            c.send(pickle.dumps("2005"))
        else:
            time.sleep(random.random()*3)
            c.send(pickle.dumps("1"))

            time.sleep(random.random()*3)
            c.send(pickle.dumps(I.Item.__init__))

            time.sleep(random.random()*3)
            c.send(pickle.dumps("0070223629"))


def send_borrow_req(c):
    answer = input("Do you want to send borrow request ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to send borrow request : ")
        user_object = find_user(email)

        time.sleep(random.random()*3)
        c.send(pickle.dumps("1"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.borrowed_req))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))


def borrow_item(c):
    answer = input("Do you want to borrow item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to borrow item : ")
        user_object = find_user(email)

        time.sleep(random.random()*3)
        c.send(pickle.dumps("1"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.borrowed_by))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))


def return_item(c):
    answer = input("Do you want to give back the item if you borrowed ? Press Y/N : ")
    if answer == "Y" or answer == "y":

        time.sleep(random.random()*3)
        c.send(pickle.dumps("0"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.returned))


def comment_item(c):
    answer = input("Do you want to comment on item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to borrow item : ")
        user_object = find_user(email)

        time.sleep(random.random()*3)
        c.send(pickle.dumps("2"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.comment))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))

        time.sleep(random.random()*3)
        c.send(pickle.dumps("Dummy Comment"))


def list_comments(c):
    answer = input("Do you want to list comments ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to list comments of item : ")
        user_object = find_user(email)

        time.sleep(random.random()*3)
        c.send(pickle.dumps("1"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.list_comments))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))


def rate_item(c):
    answer = input("Do you want to rate item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to rate item : ")
        user_object = find_user(email)

        time.sleep(random.random() * 3)
        c.send(pickle.dumps("2"))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps(I.Item.rate))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps(user_object))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps(5))


def get_rating(c):
    answer = input("Do you want to see rating of item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to see rate of item : ")
        user_object = find_user(email)

        time.sleep(random.random()*3)
        c.send(pickle.dumps("1"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.get_rating))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))


def change_location(c):
    answer = input("Do you want to change location of your item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        time.sleep(random.random()*3)
        c.send(pickle.dumps("0"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.locate))


def get_summary(c):
    answer = input("Do you want to see summary info of item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to see summary info of item : ")
        user_object = find_user(email)

        time.sleep(random.random()*3)
        c.send(pickle.dumps("1"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.view))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))


def get_detail(c):
    answer = input("Do you want to see detail info of item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to see detail info of item : ")
        user_object = find_user(email)

        time.sleep(random.random() * 3)
        c.send(pickle.dumps("1"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.detail))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps(user_object))


def change_states(c):
    answer = input("Do you want to change access states ? press Y/N : ")
    if answer == "Y" or answer == "y":
        time.sleep(random.random()*3)
        c.send(pickle.dumps("0"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(I.Item.setstate))


def announce_item(c):
    answer = input("Do you want to announce message ?  press Y/N : ")
    if answer == "Y" or answer == "y":
        answer = input("Do you want to announce to close friends(else also to friends) ? press Y/N : ")
        if answer == "Y" or answer == "y":
            time.sleep(random.random() * 3)
            c.send(pickle.dumps("2"))

            time.sleep(random.random() * 3)
            c.send(pickle.dumps(I.Item.announce))

            time.sleep(random.random() * 3)
            c.send(pickle.dumps(U.FriendshipStatus.CLOSE_FRIEND))

            time.sleep(random.random() * 3)
            c.send(pickle.dumps("Come and borrow my item"))
        else:
            time.sleep(random.random() * 3)
            c.send(pickle.dumps("2"))

            time.sleep(random.random() * 3)
            c.send(pickle.dumps(I.Item.announce))

            time.sleep(random.random() * 3)
            c.send(pickle.dumps(U.FriendshipStatus.FRIEND))

            time.sleep(random.random() * 3)
            c.send(pickle.dumps("Come and borrow my item"))


def delete_item(c):
    answer = input("Do you want to delete your item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        time.sleep(random.random() * 3)
        c.send(pickle.dumps("0"))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps(I.Item.delete))


def search_item(c):
    answer = input("Do you want to search item ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        time.sleep(random.random() * 3)
        c.send(pickle.dumps("5"))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps(I.Item.search))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps(find_user("berkantbayraktar06@gmail.com")))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps("Silahlara Veda"))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps("drama"))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps("2010"))

        time.sleep(random.random() * 3)
        c.send(pickle.dumps(False))


def list_items(c):
    answer = input("Do you want to use listItems function ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to be able to list the items of user: ")
        user_object = find_user(email)

        time.sleep(random.random()*3)
        c.send(pickle.dumps("1"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(U.User.listItems))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))


def set_friend(c):
    answer = input("Do you want to use set_friend function ? Press Y/N : ")
    if answer == "Y" or answer == "y":
        email = input("Enter an email address to be able to accept friendship request: ")
        user_object = find_user(email)

        time.sleep(random.random()*3)
        c.send(pickle.dumps("2"))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(U.User.set_friend))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(user_object))

        time.sleep(random.random()*3)
        c.send(pickle.dumps(U.FriendshipStatus.FRIEND))

    else:
        pass


def friend(c):
    answer = input("Do you want to use friend function ? Press Y/N : ")

    if answer == "Y":
        email = input("Enter an email address to be able to send friendship request: ")
        time.sleep(random.random()*3)
        c.send(pickle.dumps("1"))
        time.sleep(random.random()*3)
        c.send(pickle.dumps(U.User.friend))
        time.sleep(random.random()*3)
        c.send(pickle.dumps(email))  
        time.sleep(random.random()*3)  

    else:
        pass


def lookup(c):
    answer = input("Do you want to use lookup function ? Press Y/N : ")

    if answer == "Y":
        time.sleep(random.random() * 3)
        c.send(pickle.dumps("1"))
        time.sleep(random.random() * 3)
        c.send(pickle.dumps(U.User.lookup))
        time.sleep(random.random() * 3)
        maillist = ["ilkeraycicek36@hotmail.com","ceng@metu.edu.tr","berkantbayraktar06@gmail.com","serhatbayraktar2001@hotmail.com","yemresacma@gmail.com"]
        c.send(pickle.dumps(maillist))

    else:
        pass


def verify(email,c):
    verification_number = pickle.loads(c.recv(1024))
    print("your verification number is : ",verification_number)

    time.sleep(random.random() * 3)
    c.send(pickle.dumps("2"))
    
    time.sleep(random.random() * 3)
    c.send(pickle.dumps(U.User.verify))

    time.sleep(random.random() * 3)
    c.send(pickle.dumps(email))

    verification_number = input("Enter your verification number : ")
    time.sleep(random.random() * 3)
    c.send(pickle.dumps(verification_number))
  

def sign_up(c):
    name, password = input("Please enter name and password to sign up: ").split()

    c.send(pickle.dumps(name))
    time.sleep(random.random() * 3)

    c.send(pickle.dumps(password))
    time.sleep(random.random() * 3)
    print("You signed up as new user")


def change_password(c):
    answer = input("Do you want to change password ? Press Y/N : ")
    if answer == "Y":
        answer = input("Do you know your current password ? Press Y/N :")

        if answer == "Y":
            time.sleep(random.random()*3)
            c.send(pickle.dumps("2"))
            time.sleep(random.random()*3)
            c.send(pickle.dumps(U.User.changePassword))
            
            oldpass = input("Enter your old password : ")
            newpass = input("Enter your new password : ")

            time.sleep(random.random()*3)
            param = pickle.dumps(newpass)
            c.send(param)
            time.sleep(random.random()*3)
            param = pickle.dumps(oldpass)
            c.send(param)
            time.sleep(random.random()*3)
        else:
            time.sleep(random.random()*3)
            c.send(pickle.dumps("1"))
            time.sleep(random.random()*3)
            c.send(pickle.dumps(U.User.changePassword))
            time.sleep(random.random()*3)
            newpass = input("Enter your new password : ")
            time.sleep(random.random()*3)
            param = pickle.dumps(newpass)
            c.send(param)
            time.sleep(random.random() * 3)
            random_password = pickle.loads(c.recv(1024))
            print("your temporary password is ",random_password)
            #second call

            time.sleep(random.random()*3)
            c.send(pickle.dumps("2"))
            time.sleep(random.random()*3)
            c.send(pickle.dumps(U.User.changePassword))
            time.sleep(random.random()*3)
            param = pickle.dumps(newpass)
            c.send(param)
            time.sleep(random.random()*3)
            oldpass = input("Enter your temporary password : ")
            param = pickle.dumps(oldpass)
            c.send(param)
            time.sleep(random.random()*3)

    else:
        pass


# "ceng@metu.edu.tr"
def client(email, port):
    # send n random request
    # the connection is kept alive until client closes it.
    c = socket(AF_INET, SOCK_STREAM)
    c.connect(('127.0.0.1', port))

    try:

        c.send(pickle.dumps(email))
        time.sleep(random.random() * 3)

        reply = pickle.loads(c.recv(1024))

        if reply == "NotOK":
            sign_up(c)
            verify(email,c)
           
        else:
            print("You are authenticated user")

        
        change_password(c)
        lookup(c)
        friend(c)
        set_friend(c)
        list_items(c)
        user_watch(c)

        create_item(c)
        send_borrow_req(c)
        borrow_item(c)
        return_item(c)
        comment_item(c)
        list_comments(c)
        rate_item(c)
        get_rating(c)
        change_location(c)
        get_summary(c)
        get_detail(c)
        announce_item(c)  # Need to add user to watch list of current users item or become friend with someone
        search_item(c)
        item_watch(c)
        delete_item(c)

    finally:
        time.sleep(random.random() * 5)
        c.close()

if __name__ == "__main__":
    email = input("enter your mail address : ")
    client(email, PORT)
