from socket import *
from threading import Thread
import time
import random
import pickle

import User as U
import Item as I

PORT = 20445


def friend(c):
    answer = input("Do you want to use friend function ? Press Y/N : ")

    if answer == "Y":
        email = input("Enter an email address to be able to send friendship request: ")
        time.sleep(random.random()*3)
        c.send(pickle.dumps("1"))
        time.sleep(random.random()*3)
        c.send(pickle.dumps(U.User.friend))
        time.sleep(random.random()*3)
        c.send(pickle.dumps(email))  
        time.sleep(random.random()*3)  

    else:
        pass

def lookup(c):
    answer = input("Do you want to use lookup function ? Press Y/N : ")

    if answer == "Y":
        time.sleep(random.random() * 3)
        c.send(pickle.dumps("1"))
        time.sleep(random.random() * 3)
        c.send(pickle.dumps(U.User.lookup))
        time.sleep(random.random() * 3)
        maillist = ["ilkeraycicek36@hotmail.com","ceng@metu.edu.tr","berkantbayraktar06@gmail.com","serhatbayraktar2001@hotmail.com","yemresacma@gmail.com"]
        c.send(pickle.dumps(maillist))

    else:
        pass

def verify(email,c):
    verification_number = pickle.loads(c.recv(1024))
    print("your verification number is : ",verification_number)

    time.sleep(random.random() * 3)
    c.send(pickle.dumps("2"))
    
    time.sleep(random.random() * 3)
    c.send(pickle.dumps(U.User.verify))

    time.sleep(random.random() * 3)
    c.send(pickle.dumps(email))

    verification_number = input("Enter your verification number : ")
    time.sleep(random.random() * 3)
    c.send(pickle.dumps(verification_number))
  

def sign_up(c):
    name, password = input("Please enter name and password to sign up: ").split()

    c.send(pickle.dumps(name))
    time.sleep(random.random() * 3)

    c.send(pickle.dumps(password))
    time.sleep(random.random() * 3)
    print("You signed up as new user")


def change_password(c):
    answer = input("Do you want to change password ? Press Y/N : ")
    if answer == "Y":
        answer = input("Do you know your current password ? Press Y/N :")

        if answer == "Y":
            time.sleep(random.random()*3)
            c.send(pickle.dumps("2"))
            time.sleep(random.random()*3)
            c.send(pickle.dumps(U.User.changePassword))
            
            oldpass = input("Enter your old password : ")
            newpass = input("Enter your new password : ")

            time.sleep(random.random()*3)
            param = pickle.dumps(newpass)
            c.send(param)
            time.sleep(random.random()*3)
            param = pickle.dumps(oldpass)
            c.send(param)
            time.sleep(random.random()*3)
        else:
            time.sleep(random.random()*3)
            c.send(pickle.dumps("1"))
            time.sleep(random.random()*3)
            c.send(pickle.dumps(U.User.changePassword))
            time.sleep(random.random()*3)
            newpass = input("Enter your new password : ")
            time.sleep(random.random()*3)
            param = pickle.dumps(newpass)
            c.send(param)
            time.sleep(random.random() * 3)
            random_password = pickle.loads(c.recv(1024))
            print("your temporary password is ",random_password)
            #second call

            time.sleep(random.random()*3)
            c.send(pickle.dumps("2"))
            time.sleep(random.random()*3)
            c.send(pickle.dumps(U.User.changePassword))
            time.sleep(random.random()*3)
            param = pickle.dumps(newpass)
            c.send(param)
            time.sleep(random.random()*3)
            oldpass = input("Enter your temporary password : ")
            param = pickle.dumps(oldpass)
            c.send(param)
            time.sleep(random.random()*3)

    else:
        pass

## "ceng@metu.edu.tr"
def client(email,port):
    # send n random request
    # the connection is kept alive until client closes it.
    c = socket(AF_INET, SOCK_STREAM)
    c.connect(('127.0.0.1', port))

    try:

        c.send(pickle.dumps(email))
        time.sleep(random.random() * 3)

        reply = pickle.loads(c.recv(1024))

        if reply == "NotOK":
            sign_up(c)
            verify(email,c)
           
        else:
            print("You are authenticated user")

        change_password(c)
        lookup(c)
        friend(c)

        '''


        input("Sending friend request to yunus emre(y.emresacma@gmail.com)")
        c.send(pickle.dumps("1"))
        time.sleep(random.random()*3)
        c.send(pickle.dumps(U.User.friend))
        time.sleep(random.random()*3)
        c.send(pickle.dumps("y.emresacma@gmail.com"))  # here we will pass to second client and show it
        time.sleep(random.random()*3)                  # until accepting friend request(will be y.emresacma@gmail.com)
                                                       # additionally yunus will be open view permission and detail for this client

        input("see summary information of yunus")
        c.send(pickle.dumps("1"))
        time.sleep(random.random() * 3)
        c.send(I.Item.view)
        time.sleep(random.random() * 3)
        c.send("y.emresacma@gmail.com")
        time.sleep(random.random() * 3)

        input("see detail information of yunus")
        c.send(pickle.dumps("1"))
        time.sleep(random.random() * 3)
        c.send(I.Item.detail)
        time.sleep(random.random() * 3)
        c.send("y.emresacma@gmail.com")
        time.sleep(random.random() * 3)
       

        

        time.sleep(random.random()*3)
        c.send(pickle.dumps("1"))
        time.sleep(random.random() * 3)
        c.send(pickle.dumps(U.User.lookup))
        time.sleep(random.random() * 3)
        param = pickle.dumps(["ilkeraycicek36@hotmail.com","ceng@metu.edu.tr","yemresacma@gmail.com"])
        c.send(param)
        '''


    finally:
        time.sleep(random.random() * 5)
        c.close()


email = input("enter your mail address : ")
client(email, PORT)
