from socket import *
import threading as th
import pickle
import time
import random

import User as U
import Item as I


def find_first_item(user):
    I.Item.loadClassVariables()
    for item in I.Item.ItemList:
        if item.owner.email == user.email:
            return item


def handle_function(f, args, current_user, sock):

    print("F: ",f)
    print("ARGS: ",args)
    print(current_user)

    if f == U.User.verify:
        try:
            f(*args)
            print("User is verified.")
            U.User.saveClassVariables()
        except:
            print("User cannot verified.")

    elif f == U.User.changePassword:
        length = len(args)
        if length == 1:
            current_user.changePassword(*args)
            print("Temporary password is created : ", current_user.password)
            sock.send(pickle.dumps(current_user.password))
            U.User.saveClassVariables()

        elif length == 2:
            print("current_user , ", current_user)
            current_user.changePassword(*args)
            print("Message from server, your password changed to ", current_user.password)
            U.User.saveClassVariables()
        
    elif f == U.User.lookup:
        matchedMails = f(*args)
        print("Matched mails : " ,matchedMails)

    elif f == U.User.friend:
        current_user.friend(*args)
        for user in U.User.userList:
            if(user.email == args[0]):
                print("Friendship Request List of " + user.nameSurname + ": ", user.friendshipRequests)
        U.User.saveClassVariables()
        
    elif f == U.User.set_friend:
        print("Friendship Request List of " + current_user.nameSurname + ": ", current_user.friendshipRequests)
        current_user.set_friend(*args)
        print("Friendship Status of " + current_user.nameSurname + ": ", current_user.friendshipStatus)
        U.User.saveClassVariables()
    
    elif f == U.User.listItems:

        i = 1
        itemList = current_user.listItems(*args)
        print("ListItem method from ",current_user.email , " to ", args[0].email  ," :")

        for item in itemList:
            print ("Item number ",i ," :",item)
            i +=1

        
    elif f == U.User.watch:
        current_user.watch(*args)
        print("watchlist of ", current_user.email, ": ", current_user.watchList)
        it = I.Item(args[0],I.ItemType.DVD,"Kurtlar Vadisi",None,"Polat Alemdar","Action","2003")
        it.setstate(I.AccessType.BORROW,I.Access.EVERYONE)
        print("Notification List of ", current_user.email , current_user.notificationList)
        
        I.Item.saveClassVariables()
        U.User.saveClassVariables()

    elif f == I.Item.__init__:
        if len(args) == 1:  # only uniqid will be send from client
            new_item = I.Item(current_user, I.ItemType.BOOK, None, args[0], None, None, None)
        else:  # type, title, uniqid(will be None), artist, genre, year
            new_item = I.Item(current_user, *args)

        print("Item is created for user " + current_user.email)
        print("Created item: ", new_item)
        I.Item.saveClassVariables()

    elif f == I.Item.borrowed_req:  # user will be send
        try:
            item = find_first_item(args[0])
            item.borrowed_req(current_user)
            print(current_user.email  +" added to " + args[0].email + "'s borrow request list")
        except Exception as e:
            print(str(e))
        I.Item.saveClassVariables()

    elif f == I.Item.borrowed_by:  # user and time will be send
        try:
            item = find_first_item(args[0])
            item.borrowed_by(current_user)
            print(args[0].email + "'s item borrowed by " + current_user.email)
            I.Item.saveClassVariables()
        except Exception as e:
            print(str(e))

    elif f == I.Item.returned:
        I.Item.loadClassVariables()
        for item in I.Item.ItemList:
            if item.available[2].email == current_user.email:
                print(item.available)
                item.returned()
                print("Item returned")
                print(item.available)
                I.Item.saveClassVariables()
                break

    elif f == I.Item.comment:
        try:
            item = find_first_item(args[0])
            item.comment(current_user, args[1])
            print(item.comments)
            print("Item commented successfully")
            I.Item.saveClassVariables()

        except Exception as e:
            print(str(e))

    elif f == I.Item.list_comments:
        item = find_first_item(args[0])
        print(item.list_comments())

    elif f == I.Item.rate:
        try:
            item = find_first_item(args[0])
            item.rate(current_user, args[1])
            print("Item rated succesfully")
            I.Item.saveClassVariables()
        except Exception as e:
            print(str(e))

    elif f == I.Item.get_rating:
        try:
            item = find_first_item(args[0])
            print("Rating is: ", item.get_rating())
        except Exception as e:
            print(str(e))

    elif f == I.Item.locate:
        I.Item.loadClassVariables()
        for item in I.Item.ItemList:
            if item.available[2] is not None and item.available[2].email == current_user.email:
                item.locate(I.Location.ROOM)
                print(item.location)
                print("Location changed to room")
                I.Item.saveClassVariables()
                break

    elif f == I.Item.setstate:
        item = find_first_item(current_user)
        item.setstate(I.AccessType.BORROW, I.Access.EVERYONE)
        item.setstate(I.AccessType.VIEW, I.Access.EVERYONE)
        item.setstate(I.AccessType.COMMENT, I.Access.EVERYONE)
        item.setstate(I.AccessType.SEARCH, I.Access.EVERYONE)
        item.setstate(I.AccessType.DETAIL, I.Access.EVERYONE)
        print("All permissions opened for everyone")
        I.Item.saveClassVariables()

    elif f == I.Item.search:
        matches = I.Item.search(*args)
        for match in matches:
            print(match[0].title, " , " ,match[1].email)

    elif f == I.Item.watch:
        item = find_first_item(args[0])
        item.watch(current_user, args[1])
        print("watchlist of ", args[0].email, "'s item: ", item.watchList)
        item.setstate(I.AccessType.BORROW, I.Access.EVERYONE)
        print("Notification List of ", current_user.email, " , ", current_user.notificationList)

        I.Item.saveClassVariables()
        U.User.saveClassVariables()

    elif f == I.Item.view:
        item = find_first_item(*args)
        print(item.view(current_user))

    elif f == I.Item.detail:
        item = find_first_item(*args)
        print(item.detail(current_user))

    elif f == I.Item.announce:
        item = find_first_item(current_user)
        item.announce(*args)
        print("Notification send to watchers and " + str(args[0]) + " : " + args[1])
        I.Item.saveClassVariables()

    elif f == I.Item.delete:
        item = find_first_item(current_user)
        item.delete()
        print("Item is deleted from database and all related places")
        I.Item.saveClassVariables()


def service(sock,mutex):
    U.User.loadClassVariables()
    I.Item.loadClassVariables()
    U.User.printUserList()
    req = sock.recv(1024)
    email = pickle.loads(req)

    exist = False
    for user in U.User.userList:
        if user.email == email:
            current_user = user
            exist = True
            break

    if not exist:
        time.sleep(random.random()*3)
        sock.send(pickle.dumps("NotOK"))

        req = sock.recv(1024)
        name = pickle.loads(req)

        req = sock.recv(1024)
        password = pickle.loads(req)

        
        current_user = U.User(email, name, password)
        U.User.saveClassVariables()

        time.sleep(random.random()*3)
        sock.send(pickle.dumps(current_user.verificationCode))

        req = sock.recv(1024)
        param_num = pickle.loads(req)  # Number of parameters

        args = list()
        req = sock.recv(1024)
        f = pickle.loads(req)

        for i in range(int(param_num)):
            req = sock.recv(1024)
            item = pickle.loads(req)
            args.append(item)

        mutex.acquire()
        handle_function(f, args, current_user, sock)
        mutex.release()
        

    else:
        time.sleep(random.random()*3)
        sock.send(pickle.dumps("OK"))
        time.sleep(random.random()*3)

    while req and req != '':
        try:
            req = sock.recv(1024)
            param_num = pickle.loads(req)  # Number of parameters

            args = list()
            req = sock.recv(1024)
            f = pickle.loads(req)

            for i in range(int(param_num)):
                req = sock.recv(1024)
                item = pickle.loads(req)
                args.append(item)

            mutex.acquire()
            handle_function(f, args, current_user, sock)
            mutex.release()
            '''
            req = sock.recv(1024)
            f = pickle.loads(req)

            if f == U.User.verify:
                print("a")

            for i in range(int(param_num)):
                req = sock.recv(1024)
                item = pickle.loads(req)
                args.append(item)

            print(f(*args))
            '''
        except EOFError:
            print(sock.getpeername(), ' closing')
            break


def server(port):
    s = socket(AF_INET, SOCK_STREAM)
    s.bind(('',port))
    s.listen(50)    # 1 is queue size for "not yet accept()'ed connections"
    try:
        mutex = th.Lock()
        while True:    # just limit # of accepts for Thread to exit
            ns, peer = s.accept()
            print(peer, "connected")
            # create a thread with new socket
            t = th.Thread(target = service, args=(ns,mutex))
            t.start()
            # now main thread ready to accept next connection
    finally:
        s.close()


server = th.Thread(target=server, args=(20445,))
server.start()


