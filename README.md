Project aims to help people keeping their books, DVDs and CDs in an organized manner. The
type of items are not limited with those above but borrowing should make sense. The owner
should be able to enter his/her belongings in the system with their locations (i.e. room/shelf),
mark their borrowing status (borrowable, only borrowable to friends, not borrowable, borrowed).
Users of the system can search items of others and request borrowing, comment, watch, and
rate. Owner can update status of the item when borrowed and returned. System should
imeediately notify users for borrow requests, if a watched item becomes borrowable, and
generate some basic reports.